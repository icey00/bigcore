﻿// File:    CAccountDetail.java
// Author:  甘孝俭
// email:   ganxiaojian@hotmail.com 
// QQ:      154986287
// http://www.8088net.com
// 协议声明：本软件为开源系统，遵循国际开源组织协议。任何单位或个人可以使用或修改本软件源码，
//          可以用于作为非商业或商业用途，但由于使用本源码所引起的一切后果与作者无关。
//          未经作者许可，禁止任何企业或个人直接出售本源码或者把本软件作为独立的功能进行销售活动，
//          作者将保留追究责任的权利。
// Created: 2015/12/6 10:23:34
// Purpose: Definition of Class CAccountDetail

package com.ErpCoreModel.Store;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.Framework.CValue;

public class CAccountDetail extends CBaseObject
{

    public CAccountDetail()
    {
        TbCode = "KH_AccountDetail";
        ClassName = "com.ErpCoreModel.Store.CAccountDetail";

    }

    
        public double getScore()
        {
            if (m_arrNewVal.containsKey("score"))
                return m_arrNewVal.get("score").DoubleVal;
            else
                return 0;
        }
        public void setScore(double value)
        {
            if (m_arrNewVal.containsKey("score"))
                m_arrNewVal.get("score").DoubleVal = value;
            else
            {
                CValue val = new CValue();
                val.DoubleVal = value;
                m_arrNewVal.put("score", val);
            }
        }
        public String getContent()
        {
            if (m_arrNewVal.containsKey("content"))
                return m_arrNewVal.get("content").StrVal;
            else
                return "";
        }
        public void setContent(String value)
        {
            if (m_arrNewVal.containsKey("content"))
                m_arrNewVal.get("content").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("content", val);
            } 
       }
        public UUID getKH_Account_id()
        {
            if (m_arrNewVal.containsKey("kh_account_id"))
                return m_arrNewVal.get("kh_account_id").GuidVal;
            else
                return Util.GetEmptyUUID();
        }
        public void setKH_Account_id(UUID value)
        {
            if (m_arrNewVal.containsKey("kh_account_id"))
                m_arrNewVal.get("kh_account_id").GuidVal = value;
            else
            {
                CValue val = new CValue();
                val.GuidVal = value;
                m_arrNewVal.put("kh_account_id", val);
            }
        }
}
