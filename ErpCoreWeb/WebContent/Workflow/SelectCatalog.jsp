<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.Framework.ColumnType" %>
<%@ page import="com.ErpCoreModel.Framework.CColumnEnumVal" %>
<%@ page import="com.ErpCoreModel.Framework.CColumnMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Base.CUser" %>
<%@ page import="com.ErpCoreModel.Base.CCompany" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.UUID" %>
   
<%
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
CCompany m_Company;
String B_Company_id = request.getParameter("B_Company_id");
if (Global.IsNullParameter(B_Company_id))
    m_Company = Global.GetCtx(this.getServletContext()).getCompanyMgr().FindTopCompany();
else
    m_Company = (CCompany)Global.GetCtx(this.getServletContext()).getCompanyMgr().Find(Util.GetUUID(B_Company_id));

CTable m_Table = m_Company.getWorkflowCatalogMgr().getTable();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head >
    <title></title>
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTree.js" type="text/javascript"></script>
    <script type="text/javascript">
        var tree = null;
        $(function ()
        {
            $("#treeCatalog").ligerTree({
            url: 'SelectCatalog2.do?Action=GetData&B_Company_id=<%=request.getParameter("B_Company_id") %>',
                checkbox :false,
                onBeforeExpand: function(node) {
                    if (node.data.children && node.data.children.length == 0) {
                        tree.loadData(node.target, 'SelectCatalog2.do',
                        {
                            Action: 'GetData',
                            pid: node.data.id,
                            B_Company_id:'<%=request.getParameter("B_Company_id") %>'
                        }
                        );
                    }
                }
            });
            tree = $("#treeCatalog").ligerGetTreeManager();
        });


        function onSelect() {
            return tree.getSelected().data;
        }
    </script>
</head>
<body style="padding:10px"> 
    <div style="width:300px; height:300px; border:1px solid #ccc; overflow:auto; clear:both;">
    <ul id="treeCatalog">   </ul>
    </div>

        <div style="display:none">
     
    </div>
</body>
</html>