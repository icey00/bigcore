<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.Framework.ColumnType" %>
<%@ page import="com.ErpCoreModel.Framework.CColumnEnumVal" %>
<%@ page import="com.ErpCoreModel.Framework.CColumnMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Base.CUser" %>
<%@ page import="com.ErpCoreModel.Base.CCompany" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="com.ErpCoreModel.UI.CViewDetail" %>
<%@ page import="com.ErpCoreModel.Workflow.CWorkflowDef" %>
<%@ page import="com.ErpCoreModel.Workflow.CActivesDefMgr" %>
<%@ page import="com.ErpCoreModel.Workflow.ActivesType" %>
<%@ page import="com.ErpCoreModel.Workflow.CActivesDef" %>
<%@ page import="com.ErpCoreModel.Workflow.CLink" %>
<%@ page import="com.ErpCoreModel.Workflow.enumApprovalResult" %>
<%@ page import="com.ErpCoreModel.Workflow.CWorkflowCatalog" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.UUID" %>
    
    
<%
CUser m_User = null;
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
m_User=(CUser)request.getSession().getAttribute("User");
if (!m_User.IsRole("管理员"))
{
	response.getWriter().print("没有管理员权限！");
	response.getWriter().close();
	return ;
}
CCompany m_Company;
String B_Company_id = request.getParameter("B_Company_id");
if (Global.IsNullParameter(B_Company_id))
    m_Company = Global.GetCtx(this.getServletContext()).getCompanyMgr().FindTopCompany();
else
    m_Company = (CCompany)Global.GetCtx(this.getServletContext()).getCompanyMgr().Find(Util.GetUUID(B_Company_id));

%>
<%!

public CWorkflowDef GetWorkflowDef(HttpServletRequest request)
{
    if (request.getSession().getAttribute("AddWorkflowDef") == null)
    {
        CUser user = (CUser)request.getSession().getAttribute("User");
        CWorkflowDef WorkflowDef = new CWorkflowDef();
        WorkflowDef.Ctx = Global.GetCtx(this.getServletContext());
        UUID Catalog_id = Util.GetEmptyUUID();
        if (!Global.IsNullParameter(request.getParameter("catalog_id")))
            Catalog_id = Util.GetUUID(request.getParameter("catalog_id"));
        WorkflowDef.setWF_WorkflowCatalog_id ( Catalog_id);
        WorkflowDef.setB_Company_id ( Util.GetUUID(request.getParameter("B_Company_id")));
        WorkflowDef.setCreator (user.getId());
        CActivesDef startActivesDef = new CActivesDef();
        startActivesDef.Ctx = Global.GetCtx(this.getServletContext());
        startActivesDef.setWF_WorkflowDef_id ( WorkflowDef.getId());
        startActivesDef.setWType ( ActivesType.Start);
        startActivesDef.setName ( "启动");
        startActivesDef.setIdx ( 0);
        startActivesDef.setCreator ( user.getId());
        WorkflowDef.getActivesDefMgr().AddNew(startActivesDef);

        CActivesDef SuccessActivesDef = new CActivesDef();
        SuccessActivesDef.Ctx = Global.GetCtx(this.getServletContext());
        SuccessActivesDef.setWF_WorkflowDef_id( WorkflowDef.getId());
        SuccessActivesDef.setWType ( ActivesType.Success);
        SuccessActivesDef.setName ( "成功结束");
        SuccessActivesDef.setIdx ( -1);
        SuccessActivesDef.setCreator ( user.getId());
        WorkflowDef.getActivesDefMgr().AddNew(SuccessActivesDef);

        CActivesDef FailureActivesDef = new CActivesDef();
        FailureActivesDef.Ctx = Global.GetCtx(this.getServletContext());
        FailureActivesDef.setWF_WorkflowDef_id ( WorkflowDef.getId());
        FailureActivesDef.setWType ( ActivesType.Failure);
        FailureActivesDef.setName ( "失败结束");
        FailureActivesDef.setIdx ( -2);
        FailureActivesDef.setCreator ( user.getId());
        WorkflowDef.getActivesDefMgr().AddNew(FailureActivesDef);

        CLink Link = new CLink();
        Link.Ctx = Global.GetCtx(this.getServletContext());
        Link.setWF_WorkflowDef_id ( WorkflowDef.getId());
        Link.setResult ( enumApprovalResult.Accept);
        Link.setPreActives (startActivesDef.getId());
        Link.setNextActives (SuccessActivesDef.getId());
        Link.setCreator (user.getId());
        WorkflowDef.getLinkMgr().AddNew(Link);

        request.getSession().setAttribute("AddWorkflowDef", WorkflowDef);
    }
    return (CWorkflowDef)request.getSession().getAttribute("AddWorkflowDef");
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="../lib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script> 
    <script type="text/javascript">
        
        $(function() {
            $("#toptoolbar").ligerToolBar({ items: [
                { text: '增加', click: onAdd, icon: 'add' },
                { line: true },
                { text: '修改', click: onEdit, icon: 'modify' },
                { line: true },
                { text: '删除', click: onDelete, icon: 'delete' }
            ]
            });
        });

        function onAdd() {
            var url = 'AddActivesDef.jsp?wfid=<%=GetWorkflowDef(request).getId() %>&B_Company_id=<%=request.getParameter("B_Company_id") %>';
            $.ligerDialog.open({ title: '新建', url: url, name: 'winAddRec', height: 300, width: 400, showMax: true, showToggle: true, isResize: true, modal: false,
                buttons: [
                { text: '确定', onclick: function (item, dialog) {
                    var ret = document.getElementById('winAddRec').contentWindow.onSubmit();
                }
                },
                { text: '取消', onclick: function (item, dialog) {
                    //var ret = document.getElementById('winAddRec').contentWindow.onCancel();
                    dialog.close();
                } 
                }
             ], isResize: true
            });
        }

        function onEdit() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择行！');
                return false;
            }
            if (row.WType != "Middle") {
                $.ligerDialog.warn('系统活动不能修改！');
                return false;
            }
            var url = 'EditActivesDef.jsp?wfid=<%=GetWorkflowDef(request).getId() %>&id=' + row.id + '&B_Company_id=<%=request.getParameter("B_Company_id") %>';
            $.ligerDialog.open({ title: '修改', url: url, name: 'winEditRec', height: 300, width: 400, showMax: true, showToggle: true, isResize: true, modal: false,
                buttons: [
                { text: '确定', onclick: function(item, dialog) {
                    var ret = document.getElementById('winEditRec').contentWindow.onSubmit();
                }
                },
                { text: '取消', onclick: function(item, dialog) {
                    //var ret = document.getElementById('winEditRec').contentWindow.onCancel();
                    dialog.close();
                } }
             ], isResize: true
            });

        }
        function onDelete() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择行！');
                return false;
            }
            if (row.WType != "Middle") {
                $.ligerDialog.warn('系统活动不能删除！');
                return false;
            }
            //提交
            $.post(
                'AddWorkflowDef.do',
                {
                    Action: 'DeleteActivesDef',
                    delid: row.id,
                    B_Company_id:'<%=request.getParameter("B_Company_id") %>'
                },
                 function(data) {
                     if (data == "" || data == null) {
                         return true;
                     }
                     else {
                         return false;
                     }
                 },
                 'text');
                 
            grid.deleteRow(row);
        }

        $(function() {
            $("#toptoolbar2").ligerToolBar({ items: [
                { text: '增加', click: onAdd2, icon: 'add' },
                { line: true },
                { text: '修改', click: onEdit2, icon: 'modify' },
                { line: true },
                { text: '删除', click: onDelete2, icon: 'delete' }
            ]
            });
        });

        function onAdd2() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择活动！');
                return false;
            }
            if ($("#hidTable").val() == "") {
                $.ligerDialog.warn("请选择表对象！");
                return false;
            }
            var url = 'AddLink.jsp?wfid=<%=GetWorkflowDef(request).getId() %>&PreActives=' + row.id + '&B_Company_id=<%=request.getParameter("B_Company_id") %>';
            $.ligerDialog.open({ title: '新建', url: url, name: 'winAddRec', height: 350, width: 600, showMax: true, showToggle: true, isResize: true, modal: false,
                buttons: [
                { text: '确定', onclick: function(item, dialog) {
                    var ret = document.getElementById('winAddRec').contentWindow.onSubmit();
                }
                },
                { text: '取消', onclick: function(item, dialog) {
                    //var ret = document.getElementById('winAddRec').contentWindow.onCancel();
                    dialog.close();
                } }
             ], isResize: true
            });
        }
        function onEdit2() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择活动！');
                return false;
            }
            if ($("#hidTable").val() == "") {
                $.ligerDialog.warn("请选择表对象！");
                return false;
            }
            var row2 = grid2.getSelectedRow();
            if (row2 == null) {
                $.ligerDialog.alert('请选择连接！');
                return false;
            }
            var url = 'EditLink.jsp?wfid=<%=GetWorkflowDef(request).getId() %>&PreActives=' + row.id + '&id=' + row2.id + '&B_Company_id=<%=request.getParameter("B_Company_id") %>';
            $.ligerDialog.open({ title: '修改', url: url, name: 'winEditRec', height: 350, width: 600, showMax: true, showToggle: true, isResize: true, modal: false,
                buttons: [
                { text: '确定', onclick: function(item, dialog) {
                    var ret = document.getElementById('winEditRec').contentWindow.onSubmit();
                }
                },
                { text: '取消', onclick: function(item, dialog) {
                    //var ret = document.getElementById('winEditRec').contentWindow.onCancel();
                    dialog.close();
                } }
             ], isResize: true
            });
        }

        function onDelete2() {
            var row = grid2.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择连接！');
                return false;
            }
            //提交
            $.post(
                'AddWorkflowDef.do',
                {
                    Action: 'DeleteLink',
                    delid: row.id,
                    B_Company_id:'<%=request.getParameter("B_Company_id") %>'
                },
                 function(data) {
                     if (data == "" || data == null) {
                         return true;
                     }
                     else {
                         return false;
                     }
                 },
                 'text');

            grid2.deleteRow(row);
        }
    </script>
    
    <script type="text/javascript">
        var grid;
        $(function() {
            grid = $("#gridActives").ligerGrid({
                columns: [
                { display: '序号', name: 'Idx', width: 80 },
                { display: '活动名称', name: 'Name', width: 120, align: 'left' },
                { display: '审批类型', name: 'AType', width: 80, align: 'left' },
                { display: '审批人', name: 'UserName', width: 80, align: 'left' },
                { display: '审批角色', name: 'RoleName', width: 80, align: 'left' }
                ],
                url: 'AddWorkflowDef.do?Action=GetActivesData&B_Company_id=<%=request.getParameter("B_Company_id") %>',
                dataAction: 'server',
                usePager: false,
                width: '100%', height: '200px', allowUnSelectRow: true,
                onSelectRow: function(data, rowindex, rowobj) {
                    LoadGridLink(data.id);
                }
            });

        });
        
        var grid2;
        $(function() {
            grid2 = $("#gridLink").ligerGrid({
                columns: [
                { display: '审批结果', name: 'ResultName', width: 80 },
                { display: '转向条件', name: 'Condiction', width: 120, align: 'left' },
                { display: '后置活动', name: 'NextActivesName', width: 120, align: 'left' }
                ],
                url: '',
                dataAction: 'server',
                usePager: false,
                width: '100%', height: '200px',
                onSelectRow: function(data, rowindex, rowobj) {
                    //$.ligerDialog.alert('1选择的是' + data.id);
                }
            });

        });

        function LoadGridLink(ActivesId) {
            var url = "AddWorkflowDef.do?Action=GetLinkData&ActivesId=" + ActivesId + '&B_Company_id=<%=request.getParameter("B_Company_id") %>';
            grid2.set({ url: url});
            //grid2.loadData();
        }
        
        function onSubmit() {

            if ($("#txtName").val() == "") {
                $.ligerDialog.warn("名称不能空！");
                return false;
            }
            if ($("#hidTable").val() == "") {
                $.ligerDialog.warn("表对象不能空！");
                return false;
            }

            
            //提交
            $.post(
                'AddWorkflowDef.do',
                {
                    Action: 'PostData',
                    Name: $("#txtName").val(),
                    Catalog_id:$("#hidCatalog").val(),
                    Table_id: $("#hidTable").val(),
                    B_Company_id:'<%=request.getParameter("B_Company_id") %>'
                },
                 function(data) {
                     if (data == "" || data == null) {
                         parent.grid.loadData(true);
                         parent.$.ligerDialog.close();
                         return true;
                     }
                     else {
                         $.ligerDialog.warn(data);
                         return false;
                     }
                 },
                 'text');
            return false;
        }
        function onCancel() {

            //提交
            $.post(
                'AddWorkflowDef.do',
                {
                    Action: 'Cancel',
                    B_Company_id:'<%=request.getParameter("B_Company_id") %>'
                },
                 function(data) {
                     if (data == "" || data == null) {
                         parent.grid.loadData(true);
                         parent.$.ligerDialog.close();
                         return true;
                     }
                     else {
                         $.ligerDialog.warn(data);
                         return false;
                     }
                 },
                 'text');
            return false;
        }


        function GuidS() {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
        }
        function NewGuid() {
            var guid = (GuidS() + GuidS() + "-" + GuidS() + "-" + GuidS() + "-" + GuidS() + "-" + GuidS() + GuidS() + GuidS()).toLowerCase();
            return guid;
        }

        //下拉框
        $(function() {
            $("#cbTable").ligerComboBox({
                onBeforeOpen: onSelectTable
            });
            $("#cbCatalog").ligerComboBox({
                onBeforeOpen: onSelectCatalog
            });
        });
        function onSelectTable() {
            $.ligerDialog.open({ title: '选择表', name: 'tableselector', width: 400, height: 300, url: '../Database/Table/SelectTable.jsp', buttons: [
                { text: '确定', onclick: onSelectTableOK },
                { text: '取消', onclick: onSelectCancel }
            ]
            });
            return false;
        }
        function onSelectTableOK(item, dialog) {
            var fn = dialog.frame.onSelect || dialog.frame.window.onSelect;
            var data = fn();
            if (!data) {
                $.ligerDialog.alert('请选择行!');
                return false;
            }
            $("#cbTable").val(data.Name);
            $("#hidTable").val(data.id);

            //提交
            $.post(
                'AddWorkflowDef.do',
                {
                    Action: 'SelectTable',
                    Table_id: data.id,
                    B_Company_id:'<%=request.getParameter("B_Company_id") %>'
                },
                 function(data) {
                     if (data == "" || data == null) {
                         return true;
                     }
                     else {
                         return false;
                     }
                 },
                 'text');
                 
            dialog.close();
        }
        function onSelectCancel(item, dialog) {
            dialog.close();
        }
        function onSelectCatalog() {
            $.ligerDialog.open({ title: '选择目录', name: 'catalogselector', width: 400, height: 350, url: 'SelectCatalog.jsp?B_Company_id=<%=request.getParameter("B_Company_id") %>', buttons: [
                { text: '确定', onclick: onSelectCatalogOK },
                { text: '取消', onclick: onSelectCancel }
            ]
            });
            return false;
        }
        function onSelectCatalogOK(item, dialog) {
            var fn = dialog.frame.onSelect || dialog.frame.window.onSelect;
            var data = fn();
            if (!data) {
                $.ligerDialog.alert('请选择行!');
                return false;
            }
            $("#cbCatalog").val(data.text);
            $("#hidCatalog").val(data.id);
            dialog.close();
        }


    </script>
    
    <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>
<body style="padding:6px; overflow:hidden;">

    <table cellpadding="0" cellspacing="0" class="l-table-edit" >
        <tr>
            <td align="right" class="l-table-edit-td">名称:</td>
            <td align="left" class="l-table-edit-td"><input name="txtName" type="text" id="txtName" ltype="text" validate="{required:true,maxlength:50}" /></td>
            
            <%

            String m_sCatalogName="";
            String catalog_id = request.getParameter("catalog_id");
            if (!Global.IsNullParameter(catalog_id))
            {
                CWorkflowCatalog Catalog =(CWorkflowCatalog) m_Company.getWorkflowCatalogMgr().Find(Util.GetUUID(catalog_id));
                if (Catalog != null)
                {
                    m_sCatalogName = Catalog.getName();
                }
            }
            %>
            <td align="right" class="l-table-edit-td">目录:</td>
            <td align="left" class="l-table-edit-td"><input name="cbCatalog" type="text" id="cbCatalog" value="<%=m_sCatalogName %>"  />
            <input name="hidCatalog" type="hidden" id="hidCatalog" value="<%=request.getParameter("catalog_id") %>" /></td>
            
            <td align="right" class="l-table-edit-td">表对象:</td>
            <td align="left" class="l-table-edit-td"><input name="cbTable" type="text" id="cbTable"  />
            <input name="hidTable" type="hidden" id="hidTable"  /></td>
            
        </tr>
  </table>
  <div id="toptoolbar"></div> 
   <div id="gridActives" style="margin:0; padding:0"></div>
    
  <div id="toptoolbar2"></div> 
   <div id="gridLink" style="margin:0; padding:0"></div>
</body>
</html>
