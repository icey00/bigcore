package com.ErpCoreWeb.Menu;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Framework.CTable;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.UI.CMenu;
import com.ErpCoreModel.UI.enumMenuType;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class EditMenu
 */
@WebServlet("/EditMenu")
public class EditMenu extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;

    public CTable m_Table = null;
    public CMenu m_BaseObject = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditMenu() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
    void initData()
    {
        if (request.getSession().getAttribute("User") == null)
        {
            try {
				response.sendRedirect("../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }

		try {
			m_Table = Global.GetCtx(this.getServletContext()).getMenuMgr()
					.getTable();

			String id = request.getParameter("id");
			if (Global.IsNullParameter(id)) {
				response.getWriter().close();
				return;
			}
			m_BaseObject = (CMenu) Global.GetCtx(this.getServletContext())
					.getMenuMgr().Find(Util.GetUUID(id));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = request.getParameter("Action");
        if (Action == null) Action = "";
        
        if (Action.equalsIgnoreCase("PostData"))
        {
        	PostData();
            return ;
        }
	}

    void PostData()
    {
		try {
			String txtName = request.getParameter("txtName");
			String hidParent = request.getParameter("hidParent");
			String rdType = request.getParameter("rdType");
			String hidView = request.getParameter("hidView");
			String hidWindow = request.getParameter("hidWindow");
			String hidIcon = request.getParameter("hidIcon");
			String hidReport = request.getParameter("hidReport");
			String txtUrl = request.getParameter("txtUrl");

			String txtOpenwinWidth = request.getParameter("txtOpenwinWidth");
			String txtOpenwinHeight = request.getParameter("txtOpenwinHeight");

			if (Global.IsNullParameter(txtName)) {
				response.getWriter().print(
						"<script>parent.callback('名称不能空！')</script>");
				return;
			}
			// 同级不能有同名
			if (!txtName.equalsIgnoreCase(m_BaseObject.getName())) {
				if (Global.GetCtx(this.getServletContext()).getMenuMgr()
						.FindByName(txtName, m_BaseObject.getParent_id()) != null) {
					response.getWriter().print(
							"<script>parent.callback('菜单已经存在！')</script>");
					return;
				}
			}

			m_BaseObject.setName(txtName);
			if (hidParent.length() > 0)
				m_BaseObject.setParent_id(Util.GetUUID(hidParent));
			else
				m_BaseObject.setParent_id(Util.GetEmptyUUID());
			if (rdType.equals("0"))
				m_BaseObject.setMType(enumMenuType.CatalogMenu);
			else if (rdType.equals("1"))
				m_BaseObject.setMType(enumMenuType.ViewMenu);
			else if (rdType.equals("2"))
				m_BaseObject.setMType(enumMenuType.WindowMenu);
			else if (rdType.equals("3"))
				m_BaseObject.setMType(enumMenuType.UrlMenu);
			else if (rdType.equals("4"))
				m_BaseObject.setMType(enumMenuType.ReportMenu);
			if (hidView.length() > 0)
				m_BaseObject.setUI_View_id(Util.GetUUID(hidView));
			if (hidWindow.length() > 0)
				m_BaseObject.setUI_Window_id(Util.GetUUID(hidWindow));
			m_BaseObject.setUrl(txtUrl);
			if (hidReport.length() > 0)
				m_BaseObject.setRPT_Report_id(Util.GetUUID(hidReport));
			if (hidIcon.length() > 0)
				m_BaseObject.setIconUrl(hidIcon);
			m_BaseObject.setOpenwinWidth(Integer.valueOf(txtOpenwinWidth));
			m_BaseObject.setOpenwinHeight(Integer.valueOf(txtOpenwinHeight));

			if (!Global.GetCtx(this.getServletContext()).getMenuMgr()
					.Update(m_BaseObject, true)) {
				response.getWriter().print(
						"<script>parent.callback('修改失败！')</script>");
				return;
			}
			response.getWriter().print("<script>parent.callback('')</script>");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
