package com.ErpCoreWeb.Menu;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Base.CCompany;
import com.ErpCoreModel.Base.CUser;
import com.ErpCoreModel.Framework.CTable;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.UI.CMenu;
import com.ErpCoreModel.UI.enumMenuType;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class AddMenu
 */
@WebServlet("/AddMenu")
public class AddMenu extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;

    public CTable m_Table = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddMenu() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
    void initData()
    {
        if (request.getSession().getAttribute("User") == null)
        {
            try {
				response.sendRedirect("../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }

        m_Table = Global.GetCtx(this.getServletContext()).getMenuMgr().getTable();

    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = request.getParameter("Action");
        if (Action == null) Action = "";
        
        if (Action.equalsIgnoreCase("PostData"))
        {
        	PostData();
            return ;
        }
	}

    void PostData()
    {
		try {
			String txtName = request.getParameter("txtName");
			String hidParent = request.getParameter("hidParent");
			String rdType = request.getParameter("rdType");
			String hidView = request.getParameter("hidView");
			String hidWindow = request.getParameter("hidWindow");
			String hidIcon = request.getParameter("hidIcon");
			String hidReport = request.getParameter("hidReport");
			String txtUrl = request.getParameter("txtUrl");

			String txtOpenwinWidth = request.getParameter("txtOpenwinWidth");
			String txtOpenwinHeight = request.getParameter("txtOpenwinHeight");

			if (Global.IsNullParameter(txtName)) {
				response.getWriter().print(
						"<script>parent.callback('名称不能空！')</script>");
				return;
			}
			// 同级不能有同名
			UUID Parent_id = Util.GetEmptyUUID();
			if (!Global.IsNullParameter(hidParent))
				Parent_id = Util.GetUUID(hidParent);
			if (Global.GetCtx(this.getServletContext()).getMenuMgr()
					.FindByName(txtName, Parent_id) != null) {
				response.getWriter().print(
						"<script>parent.callback('菜单已经存在！')</script>");
				return;
			}
			CUser user = (CUser) request.getSession().getAttribute("User");

			CMenu BaseObject = new CMenu();
			BaseObject.Ctx = Global.GetCtx(this.getServletContext());
			BaseObject.setName(txtName);
			BaseObject.setParent_id(Parent_id);
			if (rdType.equalsIgnoreCase("0"))
				BaseObject.setMType(enumMenuType.CatalogMenu);
			else if (rdType.equalsIgnoreCase("1"))
				BaseObject.setMType(enumMenuType.ViewMenu);
			else if (rdType.equalsIgnoreCase("2"))
				BaseObject.setMType(enumMenuType.WindowMenu);
			else if (rdType.equalsIgnoreCase("3"))
				BaseObject.setMType(enumMenuType.UrlMenu);
			else if (rdType.equalsIgnoreCase("4"))
				BaseObject.setMType(enumMenuType.ReportMenu);
			if (Global.IsNullParameter(hidView))
				BaseObject.setUI_View_id(Util.GetUUID(hidView));
			if (Global.IsNullParameter(hidWindow))
				BaseObject.setUI_Window_id(Util.GetUUID(hidWindow));
			BaseObject.setUrl(txtUrl);
			if (Global.IsNullParameter(hidReport))
				BaseObject.setRPT_Report_id(Util.GetUUID(hidReport));
			if (Global.IsNullParameter(hidIcon))
				BaseObject.setIconUrl(hidIcon);
			BaseObject.setOpenwinWidth(Integer.valueOf(txtOpenwinWidth));
			BaseObject.setOpenwinHeight(Integer.valueOf(txtOpenwinHeight));
			BaseObject.setCreator(user.getId());

			if (!Global.GetCtx(this.getServletContext()).getMenuMgr()
					.AddNew(BaseObject, true)) {
				response.getWriter().print(
						"<script>parent.callback('添加失败！')</script>");
				return;
			}
			response.getWriter().print("<script>parent.callback('')</script>");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
