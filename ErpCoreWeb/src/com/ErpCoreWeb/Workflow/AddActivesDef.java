package com.ErpCoreWeb.Workflow;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Base.CCompany;
import com.ErpCoreModel.Base.CUser;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.CColumn;
import com.ErpCoreModel.Framework.CTable;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.UI.CView;
import com.ErpCoreModel.Workflow.CActivesDef;
import com.ErpCoreModel.Workflow.CActivesDefMgr;
import com.ErpCoreModel.Workflow.CLink;
import com.ErpCoreModel.Workflow.CWorkflowDef;
import com.ErpCoreModel.Workflow.enumApprovalResult;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class AddActivesDef
 */
@WebServlet("/AddActivesDef")
public class AddActivesDef extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;

    public CUser m_User = null;
    public CTable m_Table = null;
    public CWorkflowDef m_WorkflowDef = null;
    public CActivesDefMgr m_ActivesDefMgr = null;
    public CCompany m_Company = null; 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddActivesDef() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
    void initData()
    {
        if (request.getSession().getAttribute("User") == null)
        {
            try {
				response.sendRedirect("../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }
        m_User=(CUser)request.getSession().getAttribute("User");
        if (!m_User.IsRole("管理员"))
        {
        	try {
				response.getWriter().print("没有管理员权限！");
	        	response.getWriter().close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	return ;
        }
		try {
			String B_Company_id = request.getParameter("B_Company_id");
			if (Global.IsNullParameter(B_Company_id))
				m_Company = Global.GetCtx(this.getServletContext())
						.getCompanyMgr().FindTopCompany();
			else
				m_Company = (CCompany) Global.GetCtx(this.getServletContext())
						.getCompanyMgr().Find(Util.GetUUID(B_Company_id));

			String wfid = request.getParameter("wfid");
			if (Global.IsNullParameter(wfid)) {
				response.getWriter().close();
				return;
			}
			m_WorkflowDef = (CWorkflowDef) m_Company.getWorkflowDefMgr().Find(
					Util.GetUUID(wfid));
			if (m_WorkflowDef == null) // 可能是新建的
			{
				if (request.getSession().getAttribute("AddWorkflowDef") == null) {
					response.getWriter().close();
					return;
				}
				m_WorkflowDef = (CWorkflowDef) request.getSession()
						.getAttribute("AddWorkflowDef");
			}
			m_ActivesDefMgr = (CActivesDefMgr) m_WorkflowDef.getActivesDefMgr();

			m_Table = m_ActivesDefMgr.getTable();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = request.getParameter("Action");
        if (Action == null) Action = "";

        if (Action.equalsIgnoreCase("PostData"))
        {
        	PostData();
            return ;
        }
        else if (Action.equalsIgnoreCase("Cancel"))
        {
        	m_ActivesDefMgr.Cancel();
            return ;
        }
	}

    void PostData()
    {
        CUser user = (CUser)request.getSession().getAttribute("User");
        CActivesDef BaseObject = new CActivesDef();
        BaseObject.Ctx = Global.GetCtx(this.getServletContext());
        BaseObject.setCreator ( user.getId());
        //默认链接==
        CLink link1 = new CLink();
        link1.Ctx = Global.GetCtx(this.getServletContext());
        link1.setWF_WorkflowDef_id ( m_WorkflowDef.getId());
        link1.setPreActives ( BaseObject.getId());
        link1.setResult ( enumApprovalResult.Accept);
        CActivesDef adSuccess = m_WorkflowDef.getActivesDefMgr().FindSuccess();
        if (adSuccess != null)
            link1.setNextActives ( adSuccess.getId());
        else
            link1.setNextActives (Util.GetEmptyUUID());
        link1.setCreator ( user.getId());
        m_WorkflowDef.getLinkMgr().AddNew(link1);
        CLink link2 = new CLink();
        link2.Ctx = Global.GetCtx(this.getServletContext());
        link2.setWF_WorkflowDef_id ( m_WorkflowDef.getId());
        link2.setPreActives ( BaseObject.getId());
        link2.setResult ( enumApprovalResult.Reject);
        CActivesDef adFailure = m_WorkflowDef.getActivesDefMgr().FindFailure();
        if (adFailure != null)
            link2.setNextActives ( adFailure.getId());
        else
            link2.setNextActives ( Util.GetEmptyUUID());
        link2.setCreator ( user.getId());
        m_WorkflowDef.getLinkMgr().AddNew(link2);
        //==
        List<CBaseObject> lstCol = m_Table.getColumnMgr().GetList();
        boolean bHasVisible = false;
        for (CBaseObject obj : lstCol)
        {
            CColumn col = (CColumn)obj;

            if (col.getCode().equalsIgnoreCase("id"))
                continue;
            else if (col.getCode().equalsIgnoreCase("Created"))
                continue;
            else if (col.getCode().equalsIgnoreCase("Creator"))
                continue;
            else if (col.getCode().equalsIgnoreCase("Updated"))
                continue;
            else if (col.getCode().equalsIgnoreCase("Updator"))
                continue;

            BaseObject.SetColValue(col, request.getParameter(col.getCode()));
            bHasVisible = true;
        }
        if (!bHasVisible)
        {
            try {
				response.getWriter().print("没有可修改字段！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }
        m_ActivesDefMgr.AddNew(BaseObject);
    }

}
