package com.ErpCoreWeb.Database.Table;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Base.CUser;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.CTable;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreWeb.Common.EditObject;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class TablePanel
 */
@WebServlet("/TablePanel")
public class TablePanel extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;

	public CUser m_User = null; 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TablePanel() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
    void initData()
    {
        if (request.getSession().getAttribute("User") == null)
        {
            try {
				response.sendRedirect("../../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }
        m_User=(CUser)request.getSession().getAttribute("User");
        if (!m_User.IsRole("管理员"))
        {
        	try {
				response.getWriter().print("没有管理员权限！");
	        	response.getWriter().close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	return ;
        }

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = request.getParameter("Action");
        if (Action == null) Action = "";

        if (Action.equalsIgnoreCase("GetData"))
        {
            GetData();
            return ;
        }
        else if (Action.equalsIgnoreCase("Delete"))
        {
            DeleteTable();
            return ;
        }
	}

    void GetData()
    {
		try {
			int page = Integer.valueOf(request.getParameter("page"));
			int pageSize = Integer.valueOf(request.getParameter("pagesize"));

			String sData = "";
			List<CBaseObject> lstTable = Global
					.GetCtx(this.getServletContext()).getTableMgr().GetList();

			int totalPage = lstTable.size() % pageSize == 0 ? lstTable.size()
					/ pageSize : lstTable.size() / pageSize + 1; // 计算总页数

			int index = (page - 1) * pageSize; // 开始记录数
			for (int i = index; i < pageSize + index && i < lstTable.size(); i++) {
				CTable tb = (CTable) lstTable.get(i);

				sData += String
						.format("{ \"id\": \"%s\",\"Name\":\"%s\", \"Code\":\"%s\", \"IsSystem\":\"%b\", \"IsFinish\":\"%b\",\"Created\":\"%tF\" },",
								tb.getId().toString(), tb.getName(),
								tb.getCode(), tb.getIsSystem(),
								tb.getIsFinish(), tb.getCreated());

			}
			if (sData.endsWith(","))
				sData = sData.substring(0, sData.length() - 1);
			sData = "[" + sData + "]";
			String sJson = String.format("{\"Rows\":%s,\"Total\":\"%d\"}",
					sData, lstTable.size());

			response.getWriter().print(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    void DeleteTable()
    {
		try {
			String id = request.getParameter("id");
			if (Global.IsNullParameter(id)) {
				response.getWriter().print("请选择表！");
				return;
			}
			Global.GetCtx(this.getServletContext()).getTableMgr()
					.Delete(Util.GetUUID(id));

			if (!Global.GetCtx(this.getServletContext()).getTableMgr()
					.Save(true)) {
				response.getWriter().print("删除失败！");
				return;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
