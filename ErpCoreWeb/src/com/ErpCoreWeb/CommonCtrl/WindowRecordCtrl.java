package com.ErpCoreWeb.CommonCtrl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.CBaseObjectMgr;
import com.ErpCoreModel.Framework.CColumn;
import com.ErpCoreModel.Framework.CColumnEnumVal;
import com.ErpCoreModel.Framework.CTable;
import com.ErpCoreModel.Framework.ColumnType;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreWeb.Common.Global;

public class WindowRecordCtrl extends TagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CTable table = null;
    //受限的字段：禁止或者只读权限
    //public SortedList<Guid, AccessType> m_sortRestrictColumnAccessType = new SortedList<Guid, AccessType>();
    //界面控件列数
    public int m_iUIColCount = 2;
    //界面控件宽度
    public int m_iUICtrlWidth = 180;
    //外部传进来的引用字段或枚举字段的集合
    public Map<String, CBaseObjectMgr> m_sortRefBaseObjectMgr = new HashMap<String, CBaseObjectMgr>();
    //日期型显示时间部分的字段
    public Map<String, String> m_sortShowTimeColumn = new HashMap<String, String>();
    //下拉框弹出选择方式的字段
    public Map<String, String> m_sortPopupSelDialogColumn = new HashMap<String, String>();

    private CBaseObject baseObject = null;

	// getter and setters
	public CTable getTable() {
		return table;
	}

	public void setTable(CTable table) {
		this.table = table;
	}
	public CBaseObject getBaseObject() {
		return baseObject;
	}

	public void setBaseObject(CBaseObject BaseObject) {
		this.baseObject = BaseObject;
	}
	@Override
	public int doStartTag() throws JspException {
		try {

			JspWriter out = this.pageContext.getOut();

			if (table == null) {
				out.println("No table Found...");
				return SKIP_BODY;
			}
			
			 
			out.println("<script type=\"text/javascript\">");
			out.println("    function showEditor(id) {");
			out.println("        var editorHtml = \"#editorHtml\" + id;");
			out.println("        var editorText = \"#editorText\" + id;");
			out.println("        var txt = \"#txt_\" + id;");
			out.println("        var hid = \"#_\" + id;");

			out.println("        var dialogRet = false;");
			out.println("        $(editorHtml).dialog({");
			out.println("            title: '编辑器',");
			out.println("            width: 700,");
			out.println("            modal: true,");
			out.println("            open: function (event, ui) {");
			out.println("                // 打开Dialog后创建编辑器");
			out.println("                KindEditor.create('textarea[name=\"txt_' + id + '\"]', {");
			out.println("                    resizeType: 1,");
			out.println("                    cssPath: '../kindeditor/plugins/code/prettify.css',");
			out.println("                    uploadJson: '../kindeditor/jsp/upload_json.jsp',");
			out.println("                    fileManagerJson: '../kindeditor/jsp/file_manager_json.jsp',");
			out.println("                    allowFileManager: true");
			out.println("                });");
			out.println("            },");
			out.println("            buttons: {");
			out.println("                \"确定\": function () {");
			out.println("                    dialogRet = true;");
			out.println("                    $(this).dialog(\"close\");");
			out.println("                },");
			out.println("                \"取消\": function () {");
			out.println("                    dialogRet = false;");
			out.println("                    $(this).dialog(\"close\");");
			out.println("                }");
			out.println("            },");
			out.println("            beforeClose: function (event, ui) {");
			out.println("                // 关闭Dialog前移除编辑器");
			out.println("                KindEditor.remove('textarea[name=\"txt_' + id + '\"]');");
			out.println("                if (dialogRet)");
			out.println("                    $(hid).val($(txt).val());");
			out.println("                else");
			out.println("                    $(txt).val($(hid).val());");
			out.println("            }");
			out.println("        });");
			out.println("    }");
			out.println("    function onEditorTextChange(id) {");
			out.println("        var txt = \"#txt_\" + id;");
			out.println("        var hid = \"#_\" + id;");
			out.println("        $(txt).val($(hid).val());");
			out.println("    }");
			out.println("</script>");


			out.println("<table cellpadding=\"0\" cellspacing=\"0\" class=\"l-table-edit\" >");
			
			    int iUICol = 0;
			    List<CBaseObject> lstCol = table.getColumnMgr().GetList();
			    for (CBaseObject obj : lstCol)
			    {
			        CColumn col = (CColumn)obj;
			        //判断禁止权限字段
			        //bool bReadOnly = false;
			        //if (m_sortRestrictColumnAccessType.ContainsKey(col.Id))
			        //{
			        //    AccessType accessType = m_sortRestrictColumnAccessType[col.Id];
			        //    if (accessType == AccessType.forbide)
			        //        continue;
			        //    else if (accessType == AccessType.read)
			        //        bReadOnly = true;
			        //}
			        //
			        boolean bReadOnly = false;
			        //下拉框弹出选择方式的字段
			        boolean bPopupSelDialog = false;
			        if (m_sortPopupSelDialogColumn.containsKey(col.getCode()))
			            bPopupSelDialog = true;

			        if (col.getCode().equalsIgnoreCase("id"))
			            continue;
			        else if (col.getCode().equalsIgnoreCase("Created"))
			            continue;
			        else if (col.getCode().equalsIgnoreCase("Creator"))
			            continue;
			        else if (col.getCode().equalsIgnoreCase("Updated"))
			            continue;
			        else if (col.getCode().equalsIgnoreCase("Updator"))
			            continue;

			        //界面控件列
			        if (iUICol % m_iUIColCount == 0)
			        	out.println("<tr>");
			        iUICol++;
			        
			        if (baseObject == null)
			        {
			        
			        	out.println("<!--<tr>-->");
			        	out.println("<td align=\"right\" class=\"l-table-edit-td\">"+col.getName()+":</td>");
			        	out.println("<td align=\"left\" class=\"l-table-edit-td\">");
			                if (col.getColType() == ColumnType.string_type)
			                  { 
			                	out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\" ltype=\"text\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                }
			                  else if (col.getColType() == ColumnType.text_type)
			                  { 
			                  
			                	  out.println("<div id=\"editorText"+col.getCode()+"\">");
			                	  out.println("<textarea id=\"_"+col.getCode()+"\" name=\"_"+col.getCode()+"\" cols=\"30\" rows=\"3\" ltype=\"textarea\" style=\" width:"+m_iUICtrlWidth+"px\" onchange=\"onEditorTextChange('"+col.getCode()+"')\"></textarea>");
			                	  out.println("</div>");
			                	  out.println("<div id=\"editorHtml"+col.getCode()+"\" style=\" width:100%; height:100%; display:none\">");
			                	  out.println("<textarea id=\"txt_"+col.getCode()+"\" name=\"txt_"+col.getCode()+"\" cols=\"30\" rows=\"3\" ltype=\"textarea\" style=\" width:100%; height:100%;\" ></textarea>");
			                	  out.println("</div>");
			                	  out.println("<div><input type=\"button\" id=\"btEditor"+col.getCode()+"\" value=\"显示编辑器\" onclick=\"showEditor('"+col.getCode()+"')\"/></div>");
			                }
			                  else if (col.getColType() == ColumnType.int_type)
			                  { 
			                	  out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\" ltype=\"text\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                  }
			                  else if (col.getColType() == ColumnType.long_type)
			                  { 
			                	  out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\" ltype=\"text\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                  }
			                  else if (col.getColType() == ColumnType.bool_type)
			                  { 
			                	  out.println("<input id=\"_"+col.getCode()+"\" type=\"checkbox\" name=\"_"+col.getCode()+"\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                  }
			                  else if (col.getColType() == ColumnType.numeric_type)
			                  { 
			                	  out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\" ltype=\"text\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                  }
			                  else if (col.getColType() == ColumnType.guid_type)
			                  { 
			                	  out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\" ltype=\"text\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                  }
			                  else if (col.getColType() == ColumnType.datetime_type)
			                  { 
			                	  out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\" ltype=\"date\" validate=\"{required:true}\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                	  out.println("<script type=\"text/javascript\"> ");
			                	  out.println("$(function() {");
			                	  out.println("$(\"#_"+col.getCode()+"\").ligerDateEditor({ initValue: '', showTime: "+GetShowTimel(col) +" });");
			                	  out.println("});");
			                	  out.println("</script>");
			                  }
			                  else if (col.getColType() == ColumnType.ref_type)
			                  {
			                      if (bReadOnly)
			                      {
			                    	  out.println("<input name=\"Ref_"+col.getCode()+"\" type=\"text\" id=\"Ref_"+col.getCode()+"\" ltype=\"text\"  readonly=\"readonly\" />");
			                    	  out.println("<input name=\"_"+col.getCode()+"\" type=\"hidden\" id=\"_"+col.getCode()+"\" />");
			                    }
			                      else if (bPopupSelDialog)
			                      {
			                    	  out.println("<input name=\"Ref_"+col.getCode()+"\" type=\"text\" id=\"Ref_"+col.getCode()+"\"  ltype=\"text\"/>");
			                    	  out.println("<input name=\"_"+col.getCode()+"\" type=\"hidden\" id=\"_"+col.getCode()+"\"  />");
			                    }
			                      else
			                      { 
			                    	  out.println("<select name=\"_"+col.getCode()+"\" id=\"_"+col.getCode()+"\" ltype=\"select\">");
			                    	  out.println("<option value=\"\">(空)</option>");
			                  CTable RefTable = (CTable)Global.GetCtx(this.pageContext.getServletContext()).getTableMgr().Find(col.getRefTable());
			                    CBaseObjectMgr BaseObjectMgr = null;
			                    if (m_sortRefBaseObjectMgr.containsKey(col.getCode()))
			                        BaseObjectMgr = m_sortRefBaseObjectMgr.get(col.getCode());
			                    else
			                        BaseObjectMgr = Global.GetCtx(this.pageContext.getServletContext()).FindBaseObjectMgrCache(RefTable.getCode(), Util.GetEmptyUUID());
			                    if (BaseObjectMgr == null)
			                    {
			                        BaseObjectMgr = new CBaseObjectMgr();
			                        BaseObjectMgr.TbCode = RefTable.getCode();
			                        BaseObjectMgr.Ctx = Global.GetCtx(this.pageContext.getServletContext());
			                    }

			                    CColumn RefCol = (CColumn)RefTable.getColumnMgr().Find(col.getRefCol());
			                    CColumn RefShowCol = (CColumn)RefTable.getColumnMgr().Find(col.getRefShowCol());
			                    List<CBaseObject> lstObjRef = BaseObjectMgr.GetList();
			                    for (CBaseObject objRef : lstObjRef)
			                    { 
			                    	out.println("<option value=\""+objRef.GetColValue(RefCol) +"\">"+objRef.GetColValue(RefShowCol)+"</option>");
				                } 
			                    out.println("</select>");
			                  }
			                  }
			                  else if (col.getColType() == ColumnType.enum_type)
			                  {
			                      if (bReadOnly)
			                      {
			                    	  out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\" ltype=\"text\" value=\""+baseObject.GetColValue(col) +"\"");
			                    	  out.println("readonly=\"readonly\" />");
			                    }
			                      else
			                      { 
			                    	  out.println("<select name=\"_"+col.getCode()+"\" id=\"_"+col.getCode()+"\" ltype=\"select\">");
			                    	  out.println("<option value=\"\">(空)</option>");
			                        //引用显示字段优先
			                          if (!col.getRefShowCol().equals(Util.GetEmptyUUID()))
			                          {
			                              CTable RefTable = (CTable)Global.GetCtx(this.pageContext.getServletContext()).getTableMgr().Find(col.getRefTable());
			                              CBaseObjectMgr BaseObjectMgr = null;
			                              if (m_sortRefBaseObjectMgr.containsKey(col.getCode()))
			                                  BaseObjectMgr = m_sortRefBaseObjectMgr.get(col.getCode());
			                              else
			                                  BaseObjectMgr = Global.GetCtx(this.pageContext.getServletContext()).FindBaseObjectMgrCache(RefTable.getCode(), Util.GetEmptyUUID());
			                              if (BaseObjectMgr == null)
			                              {
			                                  BaseObjectMgr = new CBaseObjectMgr();
			                                  BaseObjectMgr.TbCode = RefTable.getCode();
			                                  BaseObjectMgr.Ctx = Global.GetCtx(this.pageContext.getServletContext());
			                              }

			                              CColumn RefShowCol = (CColumn)RefTable.getColumnMgr().Find(col.getRefShowCol());
			                              List<CBaseObject> lstObjRef = BaseObjectMgr.GetList();
			                              for (CBaseObject objRef : lstObjRef)
			                              { 
			                            	  out.println("<option value=\""+objRef.GetColValue(RefShowCol) +"\">"+objRef.GetColValue(RefShowCol)+"</option>");
				                        }
			                          }
			                          else
			                          {
			                              List<CBaseObject> lstObjEV = col.getColumnEnumValMgr().GetList();
			                              for (CBaseObject objEV : lstObjEV)
			                              {
			                                  CColumnEnumVal cev = (CColumnEnumVal)objEV;
			                             
			                                  out.println("<option value=\""+cev.getVal()+"\">"+cev.getVal()+"</option>");
			                            }
			                          } 
			                          out.println("</select>");
			                  }
			                  }
			                  else if (col.getColType() == ColumnType.object_type || col.getColType() == ColumnType.path_type)
			                  { 
			                	  out.println("<input  name=\"_"+col.getCode()+"\" id=\"_"+col.getCode()+"\" type=\"file\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                  } 
			                out.println("</td>");
			                out.println("<td align=\"left\" style=\" width:50px\">");
			                  if (!col.getAllowNull())
			                  { 
			                	  out.println("<span style=\"color:Red\">*</span>");
			                  }
			                  out.println("</td>");
			                  out.println("<!--</tr>-->");
			        }
			        else
			        {                 
			        	out.println("<!--<tr>-->");
			        	out.println("<td align=\"right\" class=\"l-table-edit-td\">"+col.getName() +":</td>");
			        	out.println("<td align=\"left\" class=\"l-table-edit-td\">");
			                if (col.getColType() == ColumnType.string_type)
			                  { 
			                	out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\" ltype=\"text\" value=\""+baseObject.GetColValue(col) +"\" ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                }
			                  else if (col.getColType() == ColumnType.text_type)
			                  { 
			                      //特殊字符处理
			                      String val = baseObject.GetColValue(col).toString();
			                      val = val.replace("\"","&quot;");
			                      val = val.replace("'","&#039;");
			                      
			                      out.println("<div id=\"editorText"+col.getCode()+"\">");
			                      out.println("<textarea id=\"_"+col.getCode()+"\" name=\"_"+col.getCode()+"\" cols=\"30\" rows=\"3\" ltype=\"textarea\" style=\" width:"+m_iUICtrlWidth+"px\" onchange=\"onEditorTextChange('"+col.getCode()+"')\">"+val+"</textarea>");
			                      out.println("</div>");
			                      out.println("<div id=\"editorHtml"+col.getCode()+"\" style=\" width:100%; height:100%;display:none\">");
			                      out.println("<textarea id=\"txt_"+col.getCode()+"\" name=\"txt_"+col.getCode()+"\" cols=\"30\" rows=\"3\" ltype=\"textarea\" style=\" width:100%; height:100%;\" >"+val+"</textarea>");
			                      out.println("</div>");
			                      out.println("<div><input type=\"button\" id=\"btEditor"+col.getCode()+"\" value=\"显示编辑器\" onclick=\"showEditor('"+col.getCode()+"')\"/></div>");
			                }
			                  else if (col.getColType() == ColumnType.int_type)
			                  { 
			                	  out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\" value=\""+baseObject.GetColValue(col) +"\" ltype=\"text\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                  }
			                  else if (col.getColType() == ColumnType.long_type)
			                  { 
			                	  out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\" value=\""+baseObject.GetColValue(col) +"\" ltype=\"text\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                  }
			                  else if (col.getColType() == ColumnType.bool_type)
			                  { 
			                	  out.println("<input id=\"_"+col.getCode()+"\" type=\"checkbox\" name=\"_"+col.getCode()+"\" ");
			                	  if((Boolean)baseObject.GetColValue(col))
			                		  out.println(" checked  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                  }
			                  else if (col.getColType() == ColumnType.numeric_type)
			                  { 
			                	  out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\" value=\""+baseObject.GetColValue(col) +"\" ltype=\"text\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                  }
			                  else if (col.getColType() == ColumnType.guid_type)
			                  { 
			                	  out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\" value=\""+baseObject.GetColValue(col) +"\" ltype=\"text\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                  }
			                  else if (col.getColType() == ColumnType.datetime_type)
			                  { 
			                	  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			                	  Date dtVal = (Date) baseObject.GetColValue(col);
			                	  String sVal = sdf.format(dtVal);
			                	  out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\"  ltype=\"date\" validate=\"{required:true}\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                	  out.println("<script type=\"text/javascript\"> ");
			                	  out.println("$(function() {");
			                	  out.println("$(\"#_"+col.getCode()+"\").ligerDateEditor({ initValue: '"+sVal +"', showTime: "+GetShowTimel(col) +" });");
			                	  out.println("});");
			                	  out.println("</script>");
			                  }
			                  else if (col.getColType() == ColumnType.ref_type)
			                  {
			                      if (bReadOnly)
			                      {
			                    	  out.println("<input name=\"Ref_"+col.getCode()+"\" type=\"text\" id=\"Ref_"+col.getCode()+"\" ltype=\"text\" value=\""+baseObject.GetRefShowColVal(col) +"\"  readonly=\"readonly\" />");
			                    	  out.println("<input name=\"_"+col.getCode()+"\" type=\"hidden\" id=\"_"+col.getCode()+"\"  value=\""+baseObject.GetColValue(col) +"\"  />");
			                    }
			                      else if (bPopupSelDialog)
			                      {
			                    	  out.println("<input name=\"Ref_"+col.getCode()+"\" type=\"text\" id=\"Ref_"+col.getCode()+"\" value=\""+baseObject.GetRefShowColVal(col) +"\" ltype=\"text\"/>");
			                    	  out.println("<input name=\"_"+col.getCode()+"\" type=\"hidden\" id=\"_"+col.getCode()+"\" value=\""+baseObject.GetColValue(col) +"\" />");
			                    }
			                      else
			                      { 
			                    	  out.println("<select name=\"_"+col.getCode()+"\" id=\"_"+col.getCode()+"\" ltype=\"select\" >");
			                    	  out.println("<option value=\"\">(空)</option>");
			                      CTable RefTable = (CTable)Global.GetCtx(this.pageContext.getServletContext()).getTableMgr().Find(col.getRefTable());
			                        CBaseObjectMgr BaseObjectMgr = null;
			                        if (m_sortRefBaseObjectMgr.containsKey(col.getCode()))
			                            BaseObjectMgr = m_sortRefBaseObjectMgr.get(col.getCode());
			                        else
			                            BaseObjectMgr = Global.GetCtx(this.pageContext.getServletContext()).FindBaseObjectMgrCache(RefTable.getCode(), Util.GetEmptyUUID());
			                        if (BaseObjectMgr == null)
			                        {
			                            BaseObjectMgr = new CBaseObjectMgr();
			                            BaseObjectMgr.TbCode = RefTable.getCode();
			                            BaseObjectMgr.Ctx = Global.GetCtx(this.pageContext.getServletContext());
			                        }

			                        CColumn RefCol = (CColumn)RefTable.getColumnMgr().Find(col.getRefCol());
			                        CColumn RefShowCol = (CColumn)RefTable.getColumnMgr().Find(col.getRefShowCol());
			                        List<CBaseObject> lstObjRef = BaseObjectMgr.GetList();
			                        for (CBaseObject objRef : lstObjRef)
			                        {
			                        	boolean bIsCurrent = baseObject.GetColValue(col).toString().equalsIgnoreCase(objRef.GetColValue(RefCol).toString());
			                            
			                            out.println("<option value=\""+objRef.GetColValue(RefCol) +"\" ");
			                            if(bIsCurrent)
			                            	out.println(" selected ");
			                            out.println(">"+objRef.GetColValue(RefShowCol)+"</option>");
				                    } 
			                        out.println("</select>");
			                  }
			                  }
			                  else if (col.getColType() == ColumnType.enum_type)
			                  {
			                      if (bReadOnly)
			                      {
			                    	  out.println("<input name=\"_"+col.getCode()+"\" type=\"text\" id=\"_"+col.getCode()+"\" ltype=\"text\" value=\""+baseObject.GetColValue(col) +"\"");
			                    	  out.println("readonly=\"readonly\" />");
			                    }
			                      else
			                      { 
			                    	  out.println("<select name=\"_"+col.getCode()+"\" id=\"_"+col.getCode()+"\" ltype=\"select\" >");
			                    	  out.println("<option value=\"\">(空)</option>");
			                        //引用显示字段优先
			                          if (!col.getRefShowCol().equals(Util.GetEmptyUUID()) )
			                          {
			                              CTable RefTable = (CTable)Global.GetCtx(this.pageContext.getServletContext()).getTableMgr().Find(col.getRefTable());
			                              CBaseObjectMgr BaseObjectMgr = null;
			                              if (m_sortRefBaseObjectMgr.containsKey(col.getCode()))
			                                  BaseObjectMgr = m_sortRefBaseObjectMgr.get(col.getCode());
			                              else
			                                  BaseObjectMgr = Global.GetCtx(this.pageContext.getServletContext()).FindBaseObjectMgrCache(RefTable.getCode(), Util.GetEmptyUUID());
			                              if (BaseObjectMgr == null)
			                              {
			                                  BaseObjectMgr = new CBaseObjectMgr();
			                                  BaseObjectMgr.TbCode = RefTable.getCode();
			                                  BaseObjectMgr.Ctx = Global.GetCtx(this.pageContext.getServletContext());
			                              }

			                              CColumn RefShowCol = (CColumn)RefTable.getColumnMgr().Find(col.getRefShowCol());
			                              List<CBaseObject> lstObjRef = BaseObjectMgr.GetList();
			                              for (CBaseObject objRef : lstObjRef)
			                              {
			                            	  boolean bIsCurrent = baseObject.GetColValue(col).toString().equalsIgnoreCase(objRef.GetColValue(RefShowCol).toString());
			                                  
			                                  out.println("<option value=\""+objRef.GetColValue(RefShowCol) +"\" ");
			                                  if(bIsCurrent)
			                                	  out.println("selected");
			                                  out.println(">"+objRef.GetColValue(RefShowCol)+"</option>");
				                        }
			                          }
			                          else
			                          {
			                              List<CBaseObject> lstObjEV = col.getColumnEnumValMgr().GetList();
			                              for (CBaseObject objEV : lstObjEV)
			                              {
			                                  CColumnEnumVal cev = (CColumnEnumVal)objEV;
			                            
			                                  out.println("<option value=\""+cev.getVal()+"\" ");
			                                  if(baseObject.GetColValue(col).toString().equalsIgnoreCase(cev.getVal()))
			                                	  out.println(" selected ");
			                                  out.println(">"+cev.getVal()+"</option>");
			                            }
			                          } 
			                          out.println(" </select>");
			                  }
			                  }
			                  else if (col.getColType() == ColumnType.object_type || col.getColType() == ColumnType.path_type)
			                  { 
			                	  out.println("<input  name=\"_"+col.getCode()+"\" id=\"_"+col.getCode()+"\" type=\"file\"  ");
			                	if(bReadOnly)
			                		out.println(" readonly=\"readonly\" ");
			                	out.println("/>");
			                	  out.println("<input type=\"checkbox\" id=\"ckClear_"+col.getCode()+"\" name=\"ckClear_"+col.getCode()+"\" />清空");
			                  } 
			                out.println("</td>");
			                out.println("<td align=\"left\" style=\" width:50px\">");
			                  if (!col.getAllowNull())
			                  { 
			                	  out.println("<span style=\"color:Red\">*</span>");
			                	  } 
			                  out.println("</td>");
			                  out.println("<!--</tr>-->");
			        }
			    }
			
			    
			    out.println("</table>");
		} catch (Exception e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	@Override
	public void release() {
		super.release();
		this.baseObject = null;
	}


	//日期型字段是否显示时间部分
    public String GetShowTimel(CColumn col)
    {
        if (m_sortShowTimeColumn.containsKey(col.getCode()))
            return "true";
        else
            return "false";
    }
}
